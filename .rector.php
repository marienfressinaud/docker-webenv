<?php

// SPDX-License-Identifier: AGPL-3.0-or-later

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\Set\ValueObject\SetList;
// use Rector\Symfony\Set\SymfonySetList;

return RectorConfig::configure()
    ->withPaths([
        // __DIR__ . '/bin',
        // __DIR__ . '/config',
        __DIR__ . '/public',
        __DIR__ . '/src',
        __DIR__ . '/tests',
    ])
    // ->withSymfonyContainerXml(__DIR__ . '/var/cache/dev/App_KernelDevDebugContainer.xml')
    ->withSets([
        SetList::PHP_82,
        SetList::PHP_83,
        SetList::TYPE_DECLARATION,
        // SymfonySetList::SYMFONY_71,
        // SymfonySetList::SYMFONY_CODE_QUALITY,
        // SymfonySetList::SYMFONY_CONSTRUCTOR_INJECTION,
    ]);
