<?php

// SPDX-License-Identifier: AGPL-3.0-or-later

declare(strict_types=1);

echo 'Hello World!<br>';

$db_host = 'database';
$db_port = 5432;
$db_username = 'postgres';
$db_password = 'postgres';
$db_dsn = "pgsql:host={$db_host};port={$db_port}";

try {
    $pdo_connection = new \PDO($db_dsn, $db_username, $db_password);
    $result = 'OK';
} catch (\PDOException $e) {
    $result = $e->getMessage();
}

echo "Database status: {$result}";
