# Webapp

**A short description of your Web application.**

[webapp.example.org](https://webapp.example.org)

---

A long description of your Web application.

It is licensed under [GNU Affero General Public License v3.0 or later](/LICENSE.txt).

## Contributing

Instructions on how to contribute (or not).

## Documentation

- [The Administrators' Guide](/docs/administrators/README.md)
- [The Developers' Guide](/docs/developers/README.md)

## Credits

Webapp relies on a bunch of other projects:

- [Composer](https://getcomposer.org/)
- [PHP](https://www.php.net/)
- [PHP\_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer)
- [PHPStan](https://phpstan.org/)
- [PHPUnit](https://phpunit.de/)
- [Rector](https://getrector.com/)

Thanks to their authors!
