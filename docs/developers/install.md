# Setup the development environment

## Setup Docker

The development environment is managed with Docker by default.

First, make sure to install [Docker Engine](https://docs.docker.com/engine/install/).
The `docker` command must be executable by your normal user.

## Install the project

Clone the repository:

```console
$ git clone <url to project>
```

Install the dependencies:

```console
$ make install
```

Start the development server:

```console
$ make docker-start
```

Open [localhost:8000](http://localhost:8000).

You can configure the port of Nginx with:

```console
$ make docker-start PORT=8080
```

A note about the `make` commands: they might feel magic, but they are not!
They are just shortcuts for common commands.
If you want to know what they do, you can open the [Makefile](/Makefile) and locates the command that you are interested in.
They are hopefully easily readable by newcomers.

## Working in the Docker containers

As the environment runs in Docker, you cannot run the `php` (or the others) directly.
There are few scripts to allow to execute commands in the Docker containers easily:

```console
$ ./docker/bin/php
$ ./docker/bin/composer
$ ./docker/bin/psql
```
