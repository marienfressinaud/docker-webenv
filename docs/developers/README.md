# The Developers' Guide

- [Setup the development environment](/docs/developers/install.md)
- [Update the development environment](/docs/developers/update.md)
- [Executing tests and linters](/docs/developers/tests.md)

Dedicated to the maintainers:

- [Reviewing the pull/merge requests](/docs/developers/review.md)
- [Managing the dependencies](/docs/developers/dependencies.md)
- [How to release a version](/docs/developers/release.md)
