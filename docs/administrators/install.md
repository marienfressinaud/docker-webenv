# Install in production

This document explains how to install the application with a “standard” setup.

In this documentation, it is expected that you're at ease with managing a webserver with PHP.
Nginx is used as the webserver in this documentation.
Apache should work as well, but it hasn't been tested.

## Installation

### Check the requirements

Check Git is installed:

```console
$ git --version
git version 2.47.0
```

Check PHP version (must be >= 8.2):

```console
$ php --version
PHP 8.2.25 ...
```

Check your database version.
If you use PostgreSQL (must be >= 15):

```console
$ psql --version
psql (PostgreSQL) 15.8
```

Check [Composer](https://getcomposer.org/) is installed:

```console
$ composer --version
Composer version 2.8.1 2024-10-04 11:31:01
```

### Create the database

Create a dedicated user and database for the application:

```command
# sudo -u postgres psql
postgres=# CREATE DATABASE webapp_production;
postgres=# CREATE USER webapp_user WITH ENCRYPTED PASSWORD 'secret';
postgres=# GRANT ALL PRIVILEGES ON DATABASE webapp_production TO webapp_user;
```

### Download the code

You may want to put the application under a specific folder on your server, for instance:

```console
$ cd /var/www/
```

Clone the code:

```console
$ git clone <url to project>
```

### About file permissions

You’ll have to make sure that the system user that runs the webserver can access the files under the application directory.
This user is often `www-data`, `apache` or `nginx`.
In this documentation, we’ll use `www-data` because it is the most generic name.

Set the owner of the files to the user that runs your webserver:

```console
$ sudo chown -R www-data:www-data .
```

From now on, you must execute the commands as the user `www-data`.
You can either start a shell for this user (to execute as root):

```console
# su www-data -s /bin/bash
www-data$ cd /var/www/webapp
```

Or prefix **all** the commands with `sudo -u www-data`.
For instance:

```console
$ sudo -u www-data php bin/console
```

If your current user is not in the sudoers list, you’ll need to execute the `sudo` commands as `root`.

The commands that need to be executed as `www-data` **will be prefixed by `www-data$` instead of simply `$` in the rest of the documentation.**

### Switch to the latest version

Checkout the code to the latest version of the application:

```
www-data$ git switch $(git describe --tags $(git rev-list --tags --max-count=1))
```

[The full list of releases.]()

### Check the PHP extensions

Check that the PHP extensions are installed:

```console
$ composer check-platform-reqs
Checking platform requirements for packages in the vendor dir
...
php           8.2.25     success
```

If requirements are not met, you’ll have to install the missing extensions.

### Install the dependencies

Install the Composer dependencies:

```console
www-data$ composer install --no-dev --optimize-autoloader
```

### Configure the webserver

Configure your webserver to serve the application.
With Nginx:

```nginx
server {
    server_name webapp.example.org;
    root /var/www/webapp/public;

    location / {
        try_files $uri /index.php$is_args$args;
    }

    location ~ ^/index\.php(/|$) {
        fastcgi_pass unix:/run/php/php8.2-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;

        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;

        internal;
    }

    location ~ \.php$ {
        return 404;
    }

    error_log /var/log/nginx/webapp_error.log;
    access_log /var/log/nginx/webapp_access.log;
}
```

Check the configuration is correct:

```console
$ nginx -t
```

And reload Nginx:

```console
$ systemctl reload nginx
```

Open the URL of the application in your web browser.

## Updating the production environment

**Please always start by checking the migration notes in [the changelog](/CHANGELOG.md) before updating the application.**

Remember that commands prefixed by `www-data$` need to be run as the `www-data` user.
[Read more about file permissions.](#about-file-permissions)

Pull the changes with Git:

```console
www-data$ git fetch
```

Switch to the latest version:

```console
www-data$ git switch $(git describe --tags $(git rev-list --tags --max-count=1))
```

Install the new/updated dependencies:

```console
www-data$ composer install --no-dev --optimize-autoloader
```
